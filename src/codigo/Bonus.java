package codigo;

import java.awt.Color;
import java.awt.Graphics;

import acm.graphics.GObject;
import acm.graphics.GOval;
import acm.graphics.GRect;

public class Bonus extends GRect{

	//contructor básico de la clase Bonus de tipo GOval
	public Bonus(double _ancho, double _alto){
		super(_ancho, _alto);
	}
	//constructor que nos servirá para darle color a nuestro GOval.
	public Bonus(double _ancho, Color _color){
		super(_ancho, _ancho);
		setFillColor(_color);
		setFilled(true);
	}
	
}
