package codigo;

import java.awt.Color;
import acm.program.*;
import java.awt.event.MouseEvent;
import acm.graphics.GRect;
import acm.util.RandomGenerator;

/*
 * Autor: Carlos Abia		1ºDAM
 * 
 * El Arkanoid pero orientado a objetos
 */

public class Arkanoid extends acm.program.GraphicsProgram{

	//características de la barra del juego
	int anchoBarra = 80;
	int altoBarra = 15; 
	Barra barra1 = new Barra(anchoBarra, altoBarra, Color.RED);
	double posicionyBarra = 0.8;
	
	//pelota del juego
	Pelota pelota1 = new Pelota(12, Color.WHITE);
	
	
	int anchoLadrillo = 30;
	int altoLadrillo = 20;
	int espacioMenu = 60;
	int anchoBarraMenu = 3;
	RandomGenerator colorRandom = new RandomGenerator();
	
	double posicionyPelota = 0.6;
	double minimo = 0.4;
	double maximo = 1;
	




	Marcador marcador = new Marcador(espacioMenu + pelota1.getWidth(), 40, this);

	public void init(){
		//color de fondo del juego
		setBackground(Color.GRAY);
		
		//añade mouse listener, necesario para usarlo
		addMouseListeners();

		//tamaño pantalla
		setSize(600, 800);		

		//barra lateral que separa menú y partida.
		GRect lateral = new GRect(anchoBarraMenu, getHeight());	
		lateral.setFilled(true);
		lateral.setFillColor(Color.BLACK);
		add(lateral, getWidth() - espacioMenu - lateral.getWidth() - pelota1.getWidth(), 0);

		//pelota del juego
		add(pelota1, (getWidth()-espacioMenu-anchoBarraMenu)/2, getHeight()*0.7);	

		//barra del juego
		add(barra1, 0 , getHeight()*posicionyBarra);
		



	}

	public void run(){
		//selecciona el nivel a dibujar
		eligeNivel();

		//empieza el juego cuando el jugador haga click
		waitForClick();

		//mientras la pelota se mantenga por encima de la barra la partida seguirá.
		while (partidaActiva(true)) {
			juega();
		}
		//resta vidas cada vez que la bola sobrepasa la línea (imaginaria) de la barra. 
		gastaVida();







	}
	//registra el movimiento del ratón para permitir que la barra le siga.
	public void mouseMoved (MouseEvent evento){
		barra1.mueveBarra(evento.getX(), getWidth()-espacioMenu-anchoBarraMenu-pelota1.getWidth());
	}
	//Nivel 1
	public void dibujaNivel01(){
		int numLadrillos = 15;

		for (int j=numLadrillos; j > 0; j-=2){
			for(int i=j; i > 0; i--){
				Ladrillo miLadrillo =
						new Ladrillo(((((getWidth()-espacioMenu-anchoLadrillo/2)-anchoLadrillo*numLadrillos)/2)
								+anchoLadrillo*i - anchoLadrillo*j/2)+getWidth()/3,
								altoLadrillo*j,
								anchoLadrillo, 
								altoLadrillo, 
								Color.BLUE); //poner colores random

				add(miLadrillo);
				pause(7);
			}
		}
			for(int k=numLadrillos; k>0; k-=2){
				for(int l=1; l>0; l--){
					Ladrillo miLadrillo =
							new Ladrillo(((((getWidth()-espacioMenu-anchoLadrillo/2)-anchoLadrillo*numLadrillos)/2)
									+anchoLadrillo*l - anchoLadrillo*k/2)+getWidth()/3,
									altoLadrillo*k,
									anchoLadrillo, 
									altoLadrillo, 
									Color.RED);
					add(miLadrillo);
					pause(7);
				}
			}
		}
	

	//Nivel 2
	public void dibujaNivel02(){
		int numLadrillos = 17;
		int numFilas = 17;
		for (int i=0; i < numFilas; i++){
			for (int j=i; j < numLadrillos; j++){
				Ladrillo miLadrillo =
						new Ladrillo(((getWidth()-espacioMenu-anchoLadrillo/2)-(anchoLadrillo*numLadrillos))/2+anchoLadrillo*j,
								altoLadrillo*i,
								anchoLadrillo, 
								altoLadrillo, 
								Color.YELLOW);
				add(miLadrillo);
				pause(7);
			}
		}
		for (int k=0; k < 1; k++){
			for (int l=0; l < numLadrillos; l++){
				Ladrillo miLadrillo =
						new Ladrillo(((getWidth()-espacioMenu-anchoLadrillo/2)-(anchoLadrillo*numLadrillos))/2+anchoLadrillo*l,
								altoLadrillo*k,
								anchoLadrillo, 
								altoLadrillo, 
								Color.RED);
				add(miLadrillo);
				pause(7);
			}
		}
	}

	//Nivel 3
	public void dibujaNivel03(){
		int numLadrillos = 14;

		for (int j=0; j < numLadrillos; j++){
			for(int i=j; i < numLadrillos; i++){
				Ladrillo miLadrillo =
						new Ladrillo(((((getWidth()-espacioMenu-18)-anchoLadrillo*numLadrillos)/2)+anchoLadrillo*i - anchoLadrillo*j/2),
								altoLadrillo*j,
								anchoLadrillo, 
								altoLadrillo, 
								Color.GREEN); //poner colores random


				add(miLadrillo);
				pause(7);
			}
		}
		for (int k=0; k < 1; k++){
			for(int l=k; l < numLadrillos; l++){
				Ladrillo miLadrillo =
						new Ladrillo(((((getWidth()-espacioMenu-18)-anchoLadrillo*numLadrillos)/2)+anchoLadrillo*l - anchoLadrillo*k/2),
								altoLadrillo*k,
								anchoLadrillo, 
								altoLadrillo, 
								Color.RED); //poner colores random


				add(miLadrillo);
				pause(7);
			}
		}
	}

	//condición que mantiene la partida activa, hasta que la pelota pasa por debajo de la barra.
	public boolean partidaActiva(boolean activa){
		if(pelota1.getY() < this.getHeight()){
			return true;
		}else return false;
	}

	public void mouseClicked(MouseEvent evento){
		//crea la funcion del botón exit, al pulsar sale del programa
		if(getElementAt(evento.getX(), evento.getY()) == marcador.cuadroSalir){
			exit();
		}	
		
		
	}
	
	//mueve la pelota y bloquea las dimensiones de la pantalla
	public void juega(){
		setSize(600, 800);
		pelota1.muevete(this);
		//bot
		//barra1.mueveBarra((int)pelota1.getX()-anchoBarra/2, getWidth()-espacioMenu-pelota1.getWidth()-anchoBarraMenu); 
		pause(2);


	}
	
	//se ejecuta al ser partidaActiva false, resta una vida, posiciona la pelota en el origen y ejecuta de nuevo
	//el run
	public void gastaVida(){
		if(marcador.contadorVidas !=0){
			pelota1.setLocation((getWidth()-espacioMenu-anchoBarraMenu)/2, getHeight()*0.7);
			marcador.actualizaVidas(1);
			pelota1.xVelocidad=0.5;
			pelota1.yVelocidad=-0.5;
			run();
		}
		
		//añade el cartel de Game Over.
		add(marcador.gameOver, getWidth()*0.1, getHeight()/2);
		setBackground(Color.black);
		marcador.exit.setColor(Color.WHITE);
	}
	
	//detecta si se ha llegado a la puntuación necesaria para pasar de nivel y si es así, resta
	//uno al nivel y resetea la puntuación.
	public void cambiaNivel(){

		if(marcador.puntuacion == 72 && marcador.contadorNivel == 1){
			marcador.actualizaNivel(1);
			marcador.puntuacion = 0;
			run();
		}
		if(marcador.puntuacion == 170 && marcador.contadorNivel == 2){
			marcador.actualizaNivel(1);
			marcador.puntuacion = 0;
			run();

		}
		if(marcador.puntuacion == 119 && marcador.contadorNivel == 3){
			//Añada el GLabel con el cartel de ganador 
			add(marcador.ganaste, getWidth()*0.1, getHeight()/2);

			//crea una sucesión de colores aleatorios en el fondo.
			while(true){
				setBackground(colorRandom.nextColor());
				pause(1);
			}

		}

	}

	/**
	 * Este void elige el nivel que tiene que dibujar atendiendo al número de nivel en el que está,
	 * si ha completado el nivel anterior o si tiene que dibujar el primer nivel porque el programa
	 * se acaba de ejecutar.
	 */
	public void eligeNivel(){
		if(marcador.contadorVidas == 3 && marcador.contadorNivel == 1){
			dibujaNivel01();
			marcador.texto.setLabel("0");
			marcador.dibuja(this);
		}
		if(marcador.contadorNivel == 2 && marcador.puntuacion==0){
			dibujaNivel02();
			marcador.puntuacion = 0;
			marcador.texto.setLabel("0");
			marcador.dibuja(this);
			pelota1.setLocation((getWidth()-espacioMenu-anchoBarraMenu)/2, getHeight()*0.7);
		}
		if(marcador.contadorNivel == 3 && marcador.puntuacion==0){
			dibujaNivel03();
			marcador.puntuacion = 0;
			marcador.texto.setLabel("0");
			marcador.dibuja(this);
			pelota1.setLocation((getWidth()-espacioMenu-anchoBarraMenu)/2, getHeight()*0.7);
		}

	}

}































