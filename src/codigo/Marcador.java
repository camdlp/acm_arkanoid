/*
 * Autor Carlos Abia Merino 	1ºDAM
 */
package codigo;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;

import javax.swing.undo.CannotUndoException;

import acm.graphics.GRect;
import acmx.export.javax.swing.JButton;
import acm.graphics.GLabel;
public class Marcador extends GRect{
	GLabel texto = new GLabel("");
	GLabel vidas = new GLabel("");
	GLabel exit = new GLabel("");
	GLabel pause = new GLabel("");
	GLabel resume = new GLabel("");
	GLabel nivel = new GLabel("");
	GRect cuadroSalir = new GRect(75, 30);
	GRect cuadroPausa = new GRect(75, 30);
	GRect cuadroResume = new GRect(75, 30);
	GLabel gameOver = new GLabel("");
	GLabel ganaste = new GLabel("");
	GLabel cartelPuntuacion = new GLabel("");
	GLabel cartelVidas = new GLabel("");
	GLabel cartelNivel = new GLabel("");


	int puntuacion = 0;
	int contadorVidas = 3;
	int contadorNivel =1;


	public Marcador(double width, double height, Arkanoid _arkanoid) {
		super(width, height);
		setFilled(true); //OJO
		setVisible(false);
		//descomenta esto si eres un flojo
		//setColor(Color.WHITE);
		//setVisible(true);	//hacer invisible el cuadro del marcador.
		texto.setLabel("0");
		texto.setFont(new Font("Impact" ,Font.CENTER_BASELINE, 24));
		vidas.setLabel(""+contadorVidas);
		vidas.setFont(new Font("Impact" ,Font.CENTER_BASELINE, 20));
		ganaste.setLabel("¡¡GANASTE!!");
		ganaste.setFont(new Font("Impact" ,Font.CENTER_BASELINE, 100));
		ganaste.setColor(Color.BLUE);
		gameOver.setLabel("GAME OVER");
		gameOver.setFont(new Font("Impact" ,Font.CENTER_BASELINE, 100));
		gameOver.setColor(Color.RED);
		cartelPuntuacion.setLabel("Puntuación");
		cartelPuntuacion.setFont(new Font("Impact" ,Font.CENTER_BASELINE, 12));
		cartelVidas.setLabel("Vidas");
		cartelVidas.setFont(new Font("Impact" ,Font.CENTER_BASELINE, 12));
		cartelNivel.setLabel("Nivel");
		cartelNivel.setFont(new Font("Impact" ,Font.CENTER_BASELINE, 12));
		nivel.setFont(new Font("Impact" ,Font.CENTER_BASELINE, 16));
		

	}

	public void dibuja(Arkanoid _arkanoid){

		texto.setLabel("0");
		vidas.setLabel(""+contadorVidas);
		exit.setLabel("EXIT");	
		exit.setColor(Color.WHITE);
		exit.setFont(new Font("Impact" ,Font.CENTER_BASELINE, 24));
		cuadroSalir.setVisible(false);
		nivel.setLabel("" +contadorNivel);


		_arkanoid.add(this,  _arkanoid.getWidth() - _arkanoid.espacioMenu -
				_arkanoid.pelota1.getWidth(), getY());//el cuadro del marcador puesto ajustado en el espacio del Menú
		

		//añade el cartel de la puntuación
		_arkanoid.add(cartelPuntuacion, _arkanoid.getWidth() - _arkanoid.espacioMenu, getY()+30);
		//añade el cartel de las vidas
		_arkanoid.add(cartelVidas, _arkanoid.getWidth() - _arkanoid.espacioMenu +10, getY()+80);
		//añade el cartel del nivel en el que se encuentra
		_arkanoid.add(cartelNivel, _arkanoid.getWidth() - _arkanoid.espacioMenu +12, getY()+130);



		//añade la puntuación en el marcador
		_arkanoid.add(texto, _arkanoid.getWidth() - _arkanoid.espacioMenu/1.5 - _arkanoid.anchoBarraMenu, getY()+60);
		//añade las vidas en el marcador
		_arkanoid.add(vidas, _arkanoid.getWidth() - _arkanoid.espacioMenu/1.5 - _arkanoid.anchoBarraMenu, getY()+110);
		//añade el nivel en el marcador
		_arkanoid.add(nivel, _arkanoid.getWidth() - _arkanoid.espacioMenu/1.5, getY()+160);
		//añade el exit en el marcador
		_arkanoid.add(exit, _arkanoid.getWidth() - _arkanoid.espacioMenu/1.5 - _arkanoid.pelota1.getWidth() - _arkanoid.anchoBarraMenu,
				_arkanoid.getHeight()-80);
		//añade el cuadro del GLabel exit
		_arkanoid.add(cuadroSalir, _arkanoid.getWidth() - _arkanoid.espacioMenu - _arkanoid.pelota1.getWidth(),
				_arkanoid.getHeight()-100);

	}

	public void actualizaMarcador(int puntos){
		puntuacion +=puntos;
		texto.setLabel("" + puntuacion);
	}
	public void actualizaVidas(int vidas){
		contadorVidas-=vidas;
		this.vidas.setLabel(""+contadorVidas);
	}
	public void actualizaNivel(int nivel){
		contadorNivel+=nivel;
		this.nivel.setLabel(""+contadorNivel);
	}
}
